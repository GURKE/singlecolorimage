/**
 * @file Fonts.cpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Delivers the fonts in multiple sizes
 * @version 2.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#include "Fonts.hpp"
#include <stdlib.h>
#include <memory>
#include <string.h>

#include "SpecialChar.hpp"

using namespace Graphics;

extern cFont Font24;
extern cFont Font20;
extern cFont Font16;
extern cFont Font12;
extern cFont Font8;

cFont* ex_Fonts[] = { &Font8, &Font12, &Font16, &Font20, &Font24 };

/* Internal font-list_-class */

Fonts& Fonts::getInstance() { // Todo -> Make const
  static Fonts fonts{};
  return fonts;
}

Fonts::Fonts() {
  for (std::uint8_t i = 0; i < MaxStoredFont + 1; i++)
    this->list_[i] = new Font(ex_Fonts[i]->Height, ex_Fonts[i]->Width, static_cast<FontSize>(i));
  for (std::uint8_t i = 0; i < 6; i++)
    this->list_[i + MaxStoredFont + 1] = new Font((i + 3) * 12, 17 + (i + 1) * 17 / 2, static_cast<FontSize>(i + 5));

  for (std::uint8_t i = 0; i < 10; i++) {
    this->list_[i]->bigger = this->list_[i + 1];
    this->list_[i + 1]->smaller =
      this->list_[i];
  }
}

std::uint8_t Font::getFontSizeUint() const {
  return static_cast<std::uint8_t>(fontSize);
}

std::uint8_t Font::fontSizeToUint(FontSize fontSize) {
  return static_cast<std::uint8_t>(fontSize);
}

Font& Fonts::getFont(FontSize fontSize) const { // TODO -> Const reference
  return getFont(Font::fontSizeToUint(fontSize));
}

Font& Fonts::getFont(std::uint8_t fontSize) const { // TODO -> Const reference
  if (fontSize < NumberOfFonts)
    return *this->list_[fontSize];
  else
    return *this->list_[NumberOfFonts - 1];
}

std::uint8_t Fonts::Height(FontSize fontSize) {
  const auto size = Font::fontSizeToUint(fontSize);
  if (size < NumberOfFonts)
    return this->list_[size]->Height;
  else
    return this->list_[NumberOfFonts - 1]->Height;
}

std::uint8_t Fonts::Width(FontSize fontSize, char c) {
  const auto size = Font::fontSizeToUint(fontSize);
  if (size < NumberOfFonts)
    return this->list_[size]->Width;
  else
    return this->list_[NumberOfFonts - 1]->Width;
}

std::uint16_t Fonts::Width(FontSize fontSize, std::vector<char>& c) {
  const auto size = Font::fontSizeToUint(fontSize);
  std::uint16_t Width = 0;
  const std::uint8_t SingleWidth = size < NumberOfFonts ? this->list_[size]->Width : this->list_[NumberOfFonts - 1]->Width;
  for (std::uint16_t i = 0; i < c.size();i++) {
    Width += SingleWidth;
  }
  return Width;
}

void Fonts::ReplaceSpecialChars(std::vector<char>& str) {
  auto backward_ptr = str.begin();
  for (auto searcher = str.begin(); searcher != str.end(); searcher++) {
    *backward_ptr = *searcher;

    if (*searcher == '\\') {
      for (SpecialChar sc : SpecialChars)
        if (strncmp(sc.Key, (const char*)&*searcher, sc.Key_Length) == 0) {
          *backward_ptr = sc.Value;
          searcher += sc.Key_Length - 1;
          break;
        }
    }

    backward_ptr++;
  }
  str.resize(backward_ptr - str.begin());
  str.shrink_to_fit();
}


/* Internal font-class */

Font::Font(std::uint8_t height, std::uint8_t width, FontSize size) : Height(height), Width(width), fontSize(size) {

}

std::unique_ptr<std::uint8_t[]> Font::getFont(char c) {
  std::unique_ptr<std::uint8_t[]> data(new std::uint8_t[((Width + 7) / 8 * Height * sizeof(std::uint8_t))]);
  const auto size = getFontSizeUint();

  if (size < Font::fontSizeToUint(FontSize::G36)) {
    memcpy(data.get(), &ex_Fonts[size]->Table[(c - ' ') * ((Width + 7) / 8 * Height)], (Width + 7) / 8 * Height);
  } else {
    //    std::logic_error("Not Implemented"); TODO
  }
  return data;
}

const std::uint8_t* Font::getFontData(std::uint8_t c) const {
  const auto size = getFontSizeUint();
  if (size <= MaxStoredFont) {
    return &ex_Fonts[size]->Table[(c - ' ') * ((Width + 7) / 8 * Height)];
  } else {
    return &ex_Fonts[NumberOfFonts]->Table[(c - ' ') * ((ex_Fonts[NumberOfFonts]->Width + 7) / 8 * ex_Fonts[NumberOfFonts]->Height)];
  }
  return nullptr;
}
