/**
 * @file SpecialChar.cpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Delivers a list of special non-ascii chars
 * @version 1.0
 * @date 2023-11-23
 *
 * Remarks:
*/
#include "SpecialChar.hpp"
#include <stdint.h>
#include <string.h>

using namespace Graphics;

namespace Graphics {
  /* 95 ASCII Char between ' ' (Space, 0d32/0x20) - '~' (Tilde, 0d126/0x7E)
   * Additional "special" chars:
   */
  namespace SpecialCharCharacters {
    constexpr std::uint8_t Degree = 0x7F; // \\deg
    constexpr std::uint8_t Sz = 0x80; // \\sz
    constexpr std::uint8_t Infty = 0x81; // \\inf
    constexpr std::uint8_t Pi = 0x82; // \\pi
    constexpr std::uint8_t RoundDownLeft = 0x83; // \\dl
    constexpr std::uint8_t RoundDownRight = 0x84; // \\dr
    constexpr std::uint8_t RoundUpLeft = 0x85; // \\ul
    constexpr std::uint8_t RoundUpRight = 0x86; // \\ur
  };

  const std::array<SpecialChar, SpecialCharCharacters::Number> SpecialChars = {
    SpecialChar("\\deg", SpecialCharCharacters::Degree),
    SpecialChar("\\sz", SpecialCharCharacters::Sz),
    SpecialChar("\\inf", SpecialCharCharacters::Infty),
    SpecialChar("\\pi", SpecialCharCharacters::Pi),
    SpecialChar("\\dl", SpecialCharCharacters::RoundDownLeft),
    SpecialChar("\\dr", SpecialCharCharacters::RoundDownRight),
    SpecialChar("\\ul", SpecialCharCharacters::RoundUpLeft),
    SpecialChar("\\ur", SpecialCharCharacters::RoundUpRight)
  };

  SpecialChar::SpecialChar(const char* _Key, std::uint8_t _Value) : Key(_Key), Value(_Value), Key_Length(strlen(Key)) {

  }
};
