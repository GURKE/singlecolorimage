/**
 * @file Image.cpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Black white image class containing multiple functions for drawing lines, circles or text
 * @version 2.0
 * @date 2023-11-23
 *
 * Remarks:
*/
#include "Image.hpp"
#include <cstdint>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "Debug.h"
#include "SpecialChar.hpp"

constexpr float PI = 3.14159265359;

using namespace Graphics;
using namespace DisplayStyle;

Image::Image() : Width_(0), Height_(0) {

}

Image::Image(std::uint16_t width, std::uint16_t height) : Width_(width), Height_(height) {
  try {
    this->bitmap = std::unique_ptr<std::uint8_t[]>(new std::uint8_t[WidthBytes() * Height_]);
  }
  catch (const std::bad_alloc& e) {
    if (DEBUG_ERROR) DEBUG_OUTPUT printf("Image konnte nicht erstellt werden, Height_=%u, Width_=%u, Code: %s\n", Height_, Width_, e.what());
    return;
  }
  for (std::uint16_t y = 0; y < Height_; y++) {
    for (std::uint16_t x = 0; x < WidthBytes(); x++) {
      this->bitmap[y * WidthBytes() + x] = 0;
    }
  }
  if (DEBUG(D_GUI_IMAGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Image created!\n");
}

Image::Image(const char* pString, Font& font, DrawColor drawColor) {
  Width_ = strlen(pString) * font.Width;
  Height_ = font.Height;
  try {
    this->bitmap = std::unique_ptr<std::uint8_t[]>(new std::uint8_t[(Width_ + (Width_ % 8 ? 1 : 0)) * Height_ / 8]);
  }
  catch (const std::bad_alloc& e) {
    if (DEBUG_ERROR) DEBUG_OUTPUT printf("Image konnte nicht erstellt werden (2), Height_=%u, Width_=%u, Code: %s\n", Height_, Width_, e.what());
    return;
  }
  this->DrawString(0, 0, pString, font, AlignHor::Left, AlignVer::Top, DrawStyle::Default, drawColor);
  if (DEBUG(D_GUI_IMAGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Image created!\n");
}

Image::~Image() {
  if (DEBUG(D_GUI_IMAGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Destroyed Image\n");
}

void Image::Clear() {
  for (std::uint32_t i = 0; i < Width_ * Height_ / 8; i++)
    this->bitmap[i] = 0;
}

void Image::SetPixel(std::uint16_t x, std::uint16_t y) {
  if (x < Width_ && y < Height_)
    this->bitmap[y * WidthBytes() + x / 8] |= 1 << (7 - (x % 8));
}

void Image::Merge(std::shared_ptr<Image> img, std::uint16_t xPoint, std::uint16_t yPoint, Mergemode mode) {
  this->Merge(xPoint, yPoint, std::move(img->bitmap), mode, img->Width_, img->Height_);
}

/**
 * @brief Reverse order of bits in a byte
 *
 * @param n Byte to be reversed
 * @retval std::uint8_t Reversed byte
*/
std::uint8_t rev_uint8(std::uint8_t n) {
  static const unsigned char lookup[16] = { 0x0, 0x8, 0x4, 0xc, 0x2, 0xa, 0x6, 0xe, 0x1, 0x9, 0x5, 0xd, 0x3, 0xb, 0x7, 0xf };
  return (lookup[n & 0b1111] << 4) | lookup[n >> 4];
}

void Image::Rotate(const Rotation rotation) {
  const std::uint16_t widthbytes = WidthBytes();
  const std::uint8_t Shift = Width_ % 8; // Bitwise mirroring

  if (rotation == Rotation::Half) {
    if (Shift) {
      std::uint8_t line_up[widthbytes];
      std::uint8_t line_down[widthbytes];
      for (std::uint16_t y = 0; y < Height_ / 2; y++) {
        memcpy(line_up, &bitmap[y * widthbytes], widthbytes);
        memcpy(line_down, &bitmap[(Height_ - y - 1) * widthbytes], widthbytes);
        for (std::uint16_t x = 0; x < widthbytes - 1; x++) {
          bitmap[(Height_ - y - 1) * widthbytes + x] = (rev_uint8(line_up[widthbytes - x - 1]) << (8 - Shift))
            | (rev_uint8(line_up[widthbytes - x - 2]) >> Shift);
          bitmap[y * widthbytes + x] = (rev_uint8(line_down[widthbytes - x - 1]) << (8 - Shift))
            | (rev_uint8(line_down[widthbytes - x - 2]) >> Shift);
        }
        bitmap[(Height_ - y - 1) * widthbytes + widthbytes - 1] = rev_uint8(line_up[0]) << (8 - Shift);
        bitmap[y * widthbytes + widthbytes - 1] = rev_uint8(line_down[0]) << (8 - Shift);
      }
      if (Height_ % 2) {
        std::uint8_t line[widthbytes];
        const std::uint8_t y = Height_ / 2;
        memcpy(line, &bitmap[y * widthbytes], widthbytes);
        for (std::uint16_t x = 0; x < widthbytes - 1; x++)
          bitmap[y * widthbytes + x] = (rev_uint8(line[widthbytes - x - 1]) << (8 - Shift))
          | (rev_uint8(line[widthbytes - x - 2]) >> Shift);
        bitmap[y * widthbytes + widthbytes - 1] = rev_uint8(line[0]) << (8 - Shift);
      }
    } else {
      for (std::uint16_t x = 0; x < (widthbytes + 1) / 2; x++) {
        for (std::uint16_t y = 0; y < Height_; y++) {
          const std::uint8_t buf = rev_uint8(bitmap[y * widthbytes + x]);
          bitmap[y * widthbytes + x] = rev_uint8(bitmap[(Height_ - y - 1) * widthbytes + (widthbytes - x - 1)]);
          bitmap[(Height_ - y - 1) * widthbytes + (widthbytes - x - 1)] = buf;
        }
      }
      if (widthbytes % 2) {
        const std::uint8_t x = widthbytes / 2;
        for (std::uint16_t y = 0; y < Height_ / 2; y++) {
          const std::uint8_t buf = rev_uint8(bitmap[y * widthbytes + x]);
          bitmap[y * widthbytes + x] = rev_uint8(bitmap[(Height_ - y - 1) * widthbytes + (widthbytes - x - 1)]);
          bitmap[(Height_ - y - 1) * widthbytes + (widthbytes - x - 1)] = buf;
        }
      }
    }
  } else {
    const std::uint16_t new_width = Height_;
    const std::uint16_t new_width_bytes = (new_width + 7) / 8;
    const std::uint16_t new_Height_ = Width_;
    const std::uint8_t new_Shift = new_width % 8; // Bitwise mirroring

    auto new_bitmap = std::unique_ptr<std::uint8_t[]>(new std::uint8_t[new_width_bytes * new_Height_]);

    for (std::uint16_t i = 0; i < new_width_bytes * new_Height_; i++)
      new_bitmap[i] = 0;

    if (rotation == Rotation::Clockwise) {
      if (DEBUG(D_GUI_ROTATE, DEBUG_DEPTH_INFO)) DEBUG_OUTPUT printf("Rotate Clockwise\n");
      for (std::uint16_t y = 0; y < Height_; y++) {
        std::uint16_t x;
        for (x = 0; x < Width_ / 8; x++)
          for (std::uint8_t i = 0; i < 8; i++)
            new_bitmap[(8 * x + i) * new_width_bytes + (new_width_bytes - 1 - (y + (8 - new_Shift) % 8) / 8)] |=
            ((bitmap[y * widthbytes + x] & (1 << (7 - i))) >> (7 - i)) << (((8 - new_Shift) % 8 + (y % 8)) % 8);

        for (std::uint8_t i = 0; i < Shift; i++)
          new_bitmap[(8 * x + i) * new_width_bytes + (new_width_bytes - 1 - (y + (8 - new_Shift) % 8) / 8)] |=
          ((bitmap[y * widthbytes + x] & (1 << (7 - i))) >> (7 - i)) << (((8 - new_Shift) % 8 + (y % 8)) % 8);
      }
    } else {
      if (DEBUG(D_GUI_ROTATE, DEBUG_DEPTH_INFO)) DEBUG_OUTPUT printf("Rotate Counterclockwise\n");
      for (std::uint16_t y = 0; y < Height_; y++) {
        std::uint16_t x;
        for (x = 0; x < Width_ / 8; x++)
          for (std::uint8_t i = 0; i < 8; i++)
            new_bitmap[(new_Height_ - 1 - (8 * x + i)) * new_width_bytes + y / 8] |=
            ((bitmap[y * widthbytes + x] & (1 << (7 - i))) << i) >> (y % 8);

        for (std::uint8_t i = 0; i < Shift; i++)
          new_bitmap[(new_Height_ - 1 - (8 * x + i)) * new_width_bytes + y / 8] |=
          ((bitmap[y * widthbytes + x] & (1 << (7 - i))) << i) >> (y % 8);
      }
    }

    Height_ = Width_;
    Width_ = new_width;
    bitmap = std::move(new_bitmap);
  }
}

void Image::Mirror(const Mirroring mirror) {
  const std::uint16_t widthbytes = WidthBytes();
  if (mirror == Mirroring::Horizontal) { // Left Right
    if (DEBUG(D_GUI_MIRROR, DEBUG_DEPTH_INFO)) DEBUG_OUTPUT printf("Mirror horizontally\n");
    const std::uint8_t Shift = Width_ % 8; // Bitwise mirroring

    if (Shift) {
      std::uint8_t line[widthbytes];
      for (std::uint16_t y = 0; y < Height_; y++) {
        memcpy(line, &bitmap[y * widthbytes], widthbytes);
        for (std::uint16_t x = 0; x < widthbytes - 1; x++)
          bitmap[y * widthbytes + x] = (rev_uint8(line[widthbytes - x - 1]) << (8 - Shift))
          | (rev_uint8(line[widthbytes - x - 2]) >> Shift);
        bitmap[y * widthbytes + widthbytes - 1] = rev_uint8(line[0]) << (8 - Shift);
      }
    } else {
      for (std::uint16_t x = 0; x < (widthbytes + 1) / 2; x++) {
        for (std::uint16_t y = 0; y < Height_; y++) {
          const std::uint8_t buf = rev_uint8(bitmap[y * widthbytes + x]);
          bitmap[y * widthbytes + x] = rev_uint8(bitmap[y * widthbytes + (widthbytes - x - 1)]);
          bitmap[y * widthbytes + (widthbytes - x - 1)] = buf;
        }
      }
    }
  } else if (mirror == Mirroring::Vertical) { // Top Down
    if (DEBUG(D_GUI_MIRROR, DEBUG_DEPTH_INFO)) DEBUG_OUTPUT printf("Mirror vertically\n");
    for (std::uint16_t y = 0; y < Height_ / 2; y++) {
      for (std::uint16_t x = 0; x < widthbytes; x++) {
        const std::uint8_t buf = bitmap[y * widthbytes + x];
        bitmap[y * widthbytes + x] = bitmap[(Height_ - y - 1) * widthbytes + x];
        bitmap[(Height_ - y - 1) * widthbytes + x] = buf;
      }
    }
  }
}

/******************** Draw Functions ********************/

void Image::DrawChar(std::uint16_t xPoint, std::uint16_t yPoint, const char asciiChar, const Font& font) {
  if (xPoint + font.Width > Width_ || yPoint + font.Height > Height_) {
    DEBUG_OUTPUT printf("Passt nicht! xPoint=%u; yPoint=%u; font.Height=%u; font.Width=%u\n", xPoint, yPoint, font.Height, font.Width);
    //return; // TODO
  }

  const std::uint8_t* const fontData = font.getFontData((std::uint8_t)asciiChar);
  const std::uint16_t CharWidthByte = (font.Width + 7) / 8;
  const std::uint8_t CharWidthOrigByte = (24 + 7) / 8;
  const std::uint8_t CharHeight_ = font.getFontSizeUint() > MaxStoredFont ? 24 : font.Height;
  const std::uint16_t ImageWidthByte = WidthBytes();
  const std::uint16_t XpointByte = xPoint / 8;
  const std::uint8_t Shift = xPoint % 8;
  const std::uint8_t ShiftedCharWidthByte = (Shift + font.Width + 7) / 8;
  const std::uint8_t scaler = font.getFontSizeUint() > MaxStoredFont ? font.getFontSizeUint() - MaxStoredFont : 0;
  const std::uint8_t new_size = font.Width * (scaler / 2.0 + 1);
  const std::uint8_t ScalerCharWidth = (font.Width * (1 + scaler / 2) + Shift + 7) / 8;

  std::uint8_t mask[ShiftedCharWidthByte] = { 0 };  // Alles 0, außer die die bestehen bleiben sollen
  mask[0] = ((std::uint8_t)(-1)) << (8 - Shift);
  if (ShiftedCharWidthByte > 1)
    mask[(xPoint % 8 + font.Width) / 8] = ((std::uint8_t)(-1)) >> ((xPoint + font.Width) % 8);

  if (DEBUG(D_GUI_DRAWCHAR, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("CharWidthByte = %u, ImageWidthByte = %u, XpointByte = %u, Shift = %u, scaler = %u, new_size = %u, ScalerCharWidth = %u\n", CharWidthByte, ImageWidthByte, XpointByte, Shift, scaler, new_size, ScalerCharWidth);

  for (std::uint16_t y = 0; y < CharHeight_; y++) {
    std::uint8_t values[ShiftedCharWidthByte] = { 0 };
    if (scaler == 0) { // font.Size <= MaxStoredFont
      for (std::uint8_t x = 0; x < CharWidthByte; x++) {
        values[x] = fontData[y * CharWidthByte + x];
      }
    } else if (scaler == 1) { // 1.5 - fach
      std::uint8_t xpos = 0;
      for (std::uint8_t x = 0; x < CharWidthOrigByte; x++) {
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 3);
        xpos++;
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 4);

        if (CharWidthOrigByte > x + 1) {
          x++;
          values[xpos] |= ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 5);
          xpos++;
          values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 0);
          xpos++;
        }
      }
    } else if (scaler == 2) { // 2-fach
      for (std::uint8_t x = 0; x < CharWidthOrigByte; x++) {
        values[2 * x + 0] = ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 4);
        values[2 * x + 1] = ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 0);
      }
    } else if (scaler == 3) { // 2.5 - fach
      std::uint8_t xpos = 0;
      for (std::uint8_t x = 0; x < CharWidthOrigByte; x++) {
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 5);
        xpos++;
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) >> 1);
        xpos++;
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 6) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 4);

        if (CharWidthOrigByte > x + 1) {
          x++;
          values[xpos] |= ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 6) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 6);
          xpos++;
          values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 3);
          xpos++;
          values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 0);
          xpos++;
        }
      }
    } else if (scaler == 4) { // 3-fach
      for (std::uint8_t x = 0; x < CharWidthOrigByte; x++) {
        values[3 * x + 0] = ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 5);
        values[3 * x + 1] = ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) >> 2);
        values[3 * x + 2] = ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 0);
      }
    } else if (scaler == 5) { // 3.5 - fach
      std::uint8_t xpos = 0;
      for (std::uint8_t x = 0; x < CharWidthOrigByte; x++) {
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 5);
        xpos++;
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 3);
        xpos++;
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) >> 1);
        xpos++;
        values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 6) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 6) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 4);

        if (CharWidthOrigByte > x + 1) {
          x++;
          values[xpos] |= ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 6) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 7);
          xpos++;
          values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 4);
          xpos++;
          values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) >> 2);
          xpos++;
          values[xpos] = ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 0);
          xpos++;
        }
      }
    } else if (scaler == 6) { // 4-fach
      for (std::uint8_t x = 0; x < CharWidthOrigByte; x++) {
        values[4 * x + 0] = ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 7)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 6)) >> 6);
        values[4 * x + 1] = ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 5)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 4)) >> 4);
        values[4 * x + 2] = ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 3)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) << 0) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) >> 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 2)) >> 2);
        values[4 * x + 3] = ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 6) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 5) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 4) | ((fontData[y * CharWidthOrigByte + x] & (1 << 1)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 3) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 2) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 1) | ((fontData[y * CharWidthOrigByte + x] & (1 << 0)) << 0);
      }
    }

    for (std::uint8_t i = ShiftedCharWidthByte - 1; i > 0; i--) {
      values[i] >>= Shift;
      values[i] |= values[i - 1] << (8 - Shift);
    }
    values[0] >>= Shift;

    int scale = 0;
    do {
      std::uint32_t array_pos = yPoint * ImageWidthByte + XpointByte;
      for (std::uint16_t x = 0; x < ShiftedCharWidthByte; x++) {
        this->bitmap[array_pos] = (this->bitmap[array_pos] & mask[x]) | (values[x]);
        array_pos++;
      }
      yPoint++;
      scale++;
    } while (scale < (scaler + 1 - y % 2) / 2 + 1);
  }
}

void Image::DrawStringV(std::uint16_t xStart, std::uint16_t yStart, std::vector<char>* pString, Font& font, AlignHor alignLR, AlignVer alignTB, DrawStyle drawStyle, DrawColor drawColor) {
  char str[pString->size() + 1];
  for (std::uint16_t i = 0; i < pString->size(); i++)
    str[i] = (*pString)[i];
  str[(*pString).size()] = '\0';
  this->DrawString(xStart, yStart, str, font, alignLR, alignTB, drawStyle, drawColor);
}

void Image::DrawString(std::uint16_t xStart, std::uint16_t yStart, const char* pString, Font& font, AlignHor alignLR, AlignVer alignTB, DrawStyle drawStyle, DrawColor drawColor) {
  std::uint8_t pString_len = strlen(pString);
  switch (alignLR) {
  case AlignHor::Center:
    xStart -= font.Width * pString_len / 2;
    break;
  case AlignHor::Right:
    xStart -= font.Width * pString_len;
    break;
  default:
    break;
  }
  switch (alignTB) {
  case AlignVer::Center:
    yStart -= font.Height / 2;
    break;
  case AlignVer::Bottom:
    yStart -= font.Height;
    break;
  default:
    break;
  }

  std::uint16_t xPoint = xStart; // Cursorposition
  std::uint16_t yPoint = yStart; // Cursorposition

  while (*pString != '\0') {
    //if X direction filled , reposition to(xStart,yPoint),yPoint is Y direction plus the Height_ of the character
    if (xPoint + font.Width > Width_ || *pString == '\n') {
      xPoint = xStart;
      yPoint += font.Height;

      // Don't start next line with space or newline
      if (*pString == ' ' || *pString == '\n') {
        pString++;
        continue;
      }
    }

    // If the Y direction is full, reposition to(xStart, yStart)
    if (yPoint + font.Height > Height_) {
      if (DEBUG_ERROR) DEBUG_OUTPUT printf("Text zu lang fuer Box, Restring:\n%s\nYpoint = %u, font.Height = %u, Height_ = %u\n", pString, yPoint, font.Height, Height_);
      return;
    }

    if (*pString == '\\') {
      for (SpecialChar sc : SpecialChars)
        if (strncmp(sc.Key, pString, strlen(sc.Key)) == 0) {
          this->DrawChar(xPoint, yPoint, sc.Value, font);
          pString += strlen(sc.Key) - 1;
          break;
        }
    } else
      this->DrawChar(xPoint, yPoint, *pString, font);


    //The next character of the address
    pString++;

    //The next word of the abscissa increases the font of the broadband
    xPoint += font.Width;
  }
  if (drawStyle == DrawStyle::Underlined)
    this->DrawLineAngle(xStart, yStart + font.Height, 90, pString_len * font.Width, 1, LineStyle::Solid, DrawColor::Black);
}



void Image::Merge(std::uint16_t xPoint, std::uint16_t yPoint, std::unique_ptr<std::uint8_t[]> _bitmap, Mergemode _mode, std::uint16_t _Width, std::uint16_t _Height_) {
  std::uint8_t Shift = xPoint % 8; // Bitwise shift
  std::uint8_t ImageWidthUp = (_Width + 7) / 8;
  std::uint16_t WidthUp = (Width_ + 7) / 8 * 8;

  if (Shift) { // Starts in the middle of a byte
    if (DEBUG(D_GUI_MERGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Shift = %u\n", Shift);
    if (8 - Shift > _Width) { // Zeichen passt innerhalb eines Bytes
      std::uint8_t ShiftBitMap = (0xFF >> Shift) & (0xFF << (8 - Shift - _Width));

      for (std::uint16_t y = 0; y < _Height_; y++) {
        if (_mode == Mergemode::Replace) this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8] &= ~ShiftBitMap; // Bits zurücksetzen
        this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8] |= (_bitmap[ImageWidthUp * y] >> Shift) & ShiftBitMap;
      }
    } else {
      std::uint8_t ShiftBitMap = 0xFF >> Shift;
      std::uint8_t Overshoot = (_Width - (8 - Shift)) % 8;
      std::uint8_t OvershootBitMap = 0xFF << (8 - Overshoot);
      std::uint8_t Full_BytesP1 = (_Width - (8 - Shift) - Overshoot) / 8 + 1;
      if (DEBUG(D_GUI_MERGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("ShiftBitMap = %u, ", ShiftBitMap);
      if (DEBUG(D_GUI_MERGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Overshoot = %u, ", Overshoot);
      if (DEBUG(D_GUI_MERGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("OvershootBitMap = %u\n", OvershootBitMap);
      if (DEBUG(D_GUI_MERGE, DEBUG_DEPTH_DEEP)) DEBUG_OUTPUT printf("Full_BytesP1 = %u\n", Full_BytesP1);

      for (std::uint16_t y = 0; y < _Height_; y++) {
        // First "splitted" Byte
        std::uint16_t x = 0;
        if (_mode == Mergemode::Replace) this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8] &= ~ShiftBitMap; // Bits zurücksetzen
        this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8] |= (_bitmap[ImageWidthUp * y] >> Shift) & ShiftBitMap;

        // Full Bytes
        if (_mode == Mergemode::Replace)
          for (x = 1; x < Full_BytesP1; x++)
            this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8 + x] = (_bitmap[ImageWidthUp * y + x - 1] << (8 - Shift)) | (_bitmap[ImageWidthUp * y + x] >> Shift);
        else
          for (x = 1; x < Full_BytesP1; x++)
            this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8 + x] |= (_bitmap[ImageWidthUp * y + x - 1] << (8 - Shift)) | (_bitmap[ImageWidthUp * y + x] >> Shift);

        // Last splitted Byte
        if (Overshoot) {
          if (_mode == Mergemode::Replace) this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8 + x] &= ~OvershootBitMap; // Bits zurücksetzen
          this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8 + x] |= ((_bitmap[ImageWidthUp * y + x - 1] << (8 - Shift)) | (_bitmap[ImageWidthUp * y + x] >> Shift)) & OvershootBitMap;
        }
      }
    }
  } else { // Starts at the beginning of a byte
    std::uint8_t ImageWidth = _Width / 8;
    std::uint8_t Overshoot = _Width % 8;
    std::uint8_t OvershootBitMap = 0xFF << (8 - Overshoot);
    for (std::uint16_t y = 0; y < _Height_; y++) {
      if (_mode == Mergemode::Replace)
        for (std::uint16_t x = 0; x < ImageWidth; x++)
          this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8 + x] = _bitmap[y * ImageWidthUp + x];
      else
        for (std::uint16_t x = 0; x < ImageWidth; x++)
          this->bitmap[((y + yPoint) * WidthUp + xPoint) / 8 + x] |= _bitmap[y * ImageWidthUp + x];

      if (Overshoot) {
        std::uint32_t pos = ((y + yPoint) * WidthUp + xPoint) / 8 + ImageWidth;
        if (_mode == Mergemode::Replace) this->bitmap[pos] &= ~OvershootBitMap; // Bits zurücksetzen
        this->bitmap[pos] |= _bitmap[y * ImageWidthUp + ImageWidth] & OvershootBitMap; // Neue Bits setzen
      }
    }
  }
}

void Image::DrawLine(std::uint16_t xStart, std::uint16_t yStart, std::uint16_t Xend, std::uint16_t Yend, std::uint8_t Line_Width, DisplayStyle::LineStyle Line_Style, DisplayStyle::DrawColor drawColor) {
  if (xStart == Xend)
    DrawLineAngle(xStart, yStart, Yend > yStart ? 180 : 0, abs(Yend - yStart), Line_Width, Line_Style, drawColor);
  else if (yStart == Yend)
    DrawLineAngle(xStart, yStart, Xend > xStart ? 90 : 270, abs(Xend - xStart), Line_Width, Line_Style, drawColor);
  else {
    std::uint16_t angle;
    if (abs(Xend - xStart) == abs(Yend - yStart)) // 45° / 135° / 225° / 315°
      angle = Xend > xStart ? (Yend > yStart ? 135 : 45) : (Yend > yStart ? 225 : 315);
    else
      angle = (Yend - yStart > 0 ? 180 : 0) - atan((Xend - xStart) / (Yend - yStart)) / PI * 180;

    DrawLineAngle(xStart, yStart, angle, sqrt(pow(Xend - xStart, 2) + pow(Yend - yStart, 2)), Line_Width, Line_Style, drawColor);
  }
}

std::uint16_t Image::CalcLine(std::uint16_t* Y, std::uint16_t Yend, std::uint16_t xStart, std::uint16_t Xend) {
  const float Delta_Y = (Yend - Y[0]) * 1.0 / (Xend - xStart);
  float Y_c = Y[0] + Delta_Y;
  std::uint16_t X_c;
  for (X_c = 1; X_c <= Xend - xStart; X_c++, Y_c += Delta_Y)
    Y[X_c] = (std::uint16_t)Y_c;
  return X_c;
}

std::uint16_t Image::Calc_Delta_X(const std::uint16_t Delta_Y, const std::uint16_t* Y, const std::uint16_t Len) {
  std::uint16_t DeltaX = 0;
  if (Y[0] > Y[Len])
    while (DeltaX < Len && Y[0] <= Y[DeltaX] + Delta_Y)
      DeltaX++;
  else
    while (DeltaX < Len&& Y[0] >= Y[DeltaX] - Delta_Y)
      DeltaX++;

  return DeltaX;
}

std::uint16_t Image::Calc_Delta_Y(std::uint8_t Line_Width, const std::uint16_t* Y_U, const std::uint16_t* Y_D, std::uint16_t Len) {
  const bool U_G_D = Y_U[0] > Y_D[0];
  std::uint16_t Delta_Y = (U_G_D ? (Y_U[0] - Y_D[0]) : (Y_D[0] - Y_U[0])) + 1 + 1; // Length is 1 greater than distance and Y is decreased in loop by one
  std::uint16_t Delta_X;
  float h = 0;
  do {
    Delta_Y--;
    Delta_X = Calc_Delta_X(Delta_Y, Y_U, Len);
  } while ((h = fabs(Delta_X * (Delta_Y + 1) * 1.0 / sqrt(Delta_X * Delta_X + (Delta_Y + 1) * (Delta_Y + 1)))) > (float)Line_Width);
  return Delta_Y;
}

void Image::DrawLineAngle(std::uint16_t xStart, std::uint16_t yStart, std::uint16_t angle, std::uint16_t Radius, std::uint8_t Line_Width, DisplayStyle::LineStyle Line_Style, DisplayStyle::DrawColor drawColor) {
  angle = angle % 360;

  if ((angle + 45) % 90 == 0) { // 45° Diagonal
    std::uint16_t Len = Radius / sqrt(2) + 1;

    // Line Width_ Correction for a better line width
    if (Line_Width > 1)
      Line_Width = ((std::uint16_t)((Line_Width + 1) / 1.4142 * 1.2857)) * 1.4142 - 1 + 0.5;

    const bool downwards = angle == 135 || angle == 315;
    std::uint16_t XS = xStart - Line_Width / 4;
    std::uint16_t YS = yStart + (downwards ? 1 : -1) * (Line_Width + 2) / 4;

    // Start coordinate correction
    switch (angle) {
    case 45:
    case 135:
      XS++;
      YS++;
      break;
    case 225:
      XS -= Len - 1;
      YS += Len + 1;
      break;
    case 315:
      XS -= Len - 1;
      YS -= Len - 1;
      break;
    default:
      return;
    }

    std::uint8_t Next_Step = Line_Width;

    for (std::uint16_t i = 0; i < Line_Width; i++) {
      for (std::uint16_t j = 0; j < Len; j++)
        this->DrawPoint(XS + j, downwards ? YS + j : YS - j, 1, DisplayStyle::DotStyle::Around, drawColor);
      if (Next_Step % 2)
        XS++;
      else
        YS = downwards ? YS - 1 : YS + 1;
      Next_Step++;
    }
    return;
  } else if (angle % 90 == 0) { // Straight lines
    std::uint16_t XS, XE, YS, YE;
    switch (angle) {
    case 0:
      XS = xStart - Line_Width / 2;
      XE = xStart + Line_Width / 2 - ((Line_Width + 1) % 2);
      YS = yStart - Radius;
      YE = yStart;
      break;
    case 90:
      XS = xStart;
      XE = xStart + Radius;
      YS = yStart - Line_Width / 2;
      YE = yStart + Line_Width / 2 - ((Line_Width + 1) % 2);
      break;
    case 180:
      XS = xStart - Line_Width / 2;
      XE = xStart + Line_Width / 2 - ((Line_Width + 1) % 2);
      YS = yStart;
      YE = yStart + Radius;
      break;
    case 270:
      XS = xStart - Radius;
      XE = xStart;
      YS = yStart - Line_Width / 2;
      YE = yStart + Line_Width / 2 - ((Line_Width + 1) % 2);
      break;
    }

    for (std::uint16_t y = YS; y <= YE; y++)
      for (std::uint16_t x = XS; x <= XE; x++)
        this->DrawPoint(x, y, 1, DisplayStyle::DotStyle::Around, drawColor);
    return;
  } else { // Crooked line
    std::uint16_t XS, XE, YS, YE;
    const bool X_Wider_Y = (angle > 45 && angle < 135) || (angle > 225 && angle < 315);

    // If Delta Y is wider than Delta X, switch X and Y to use the same calculation as if Delta X would be wider than Delta Y
    // Switch Start and End coordinates if Start is to the right of End
    if (X_Wider_Y) { // Flat
      if (angle < 180) { // From Left to Right
        XS = xStart;
        YS = yStart;
        XE = xStart + Radius * sin(angle * PI / 180);
        YE = yStart - Radius * cos(angle * PI / 180);
      } else { // From Right to Left
        XS = xStart + Radius * sin(angle * PI / 180);
        YS = yStart - Radius * cos(angle * PI / 180);
        XE = xStart;
        YE = yStart;
      }
    } else { // Steep -> Exchange X and Y coordinate to get a flat line which is inverted afterwards again
      if (angle <= 45) {
        XS = yStart - Radius * cos(angle * PI / 180);
        YS = xStart + Radius * sin(angle * PI / 180);
        XE = yStart;
        YE = xStart;
      } else if (angle >= 135 && angle < 180) {
        XS = yStart;
        YS = xStart;
        XE = yStart - Radius * cos(angle * PI / 180);
        YE = xStart + Radius * sin(angle * PI / 180);
      } else if (angle > 180 && angle <= 225) {
        XS = yStart;
        YS = xStart;
        XE = yStart - Radius * cos(angle * PI / 180);
        YE = xStart + Radius * sin(angle * PI / 180);
      } else { // angle > 315
        XS = yStart - Radius * cos(angle * PI / 180);
        YS = xStart + Radius * sin(angle * PI / 180);
        XE = yStart;
        YE = xStart;
      }
    }

    // xStart = left, Delta_X > Delta_Y
    // | /  0 > angle > -pi/2
    // S -  angle = 0
    // | \  pi/2 > angle > 0

    float fangle = atan((YE - YS) * 1.0 / (XE - XS));

    const float sin_angle = sin(fangle);
    const int16_t x_diagonal = (float)Line_Width * sin_angle + (sin_angle < 0 ? -0.5 : 0.5);
    const std::uint16_t y_diagonal = (float)Line_Width * cos(fangle) + 0.5;
    const std::uint16_t Len = abs(XE - XS) + abs(x_diagonal);

    // Upper Y-Values for every
    std::uint16_t Y_U[Len];
    // Downer Y-Values for every
    std::uint16_t Y_D[Len];

    const std::uint16_t Xstart_U = XS + x_diagonal / 2;
    const std::uint16_t Xstart_D = XS - x_diagonal / 2;
    const std::uint16_t Xend_U = XE + x_diagonal / 2;
    const std::uint16_t Xend_D = XE - x_diagonal / 2;
    const std::uint16_t Ystart_U = YS - y_diagonal / 2;
    const std::uint16_t Ystart_D = YS + y_diagonal / 2;
    const std::uint16_t Yend_U = YE - y_diagonal / 2;
    const std::uint16_t Yend_D = YE + y_diagonal / 2;

    if (Xstart_U == Xstart_D) {
      Y_U[0] = Ystart_U;
      CalcLine(Y_U, Yend_U, Xstart_U, Xend_U);
      const std::uint16_t Delta_Y_M = Ystart_D - Ystart_U;
      for (std::uint16_t i = 0; i < Len; i++)
        Y_D[i] = Y_U[i] + Delta_Y_M;
    } else {
      if (fangle < 0) {
        // Calculate main line down
        Y_U[0] = Y_D[0] = Ystart_U;
        CalcLine(Y_U, Yend_U, Xstart_U, Xend_U);

        // Calculate left diagonally line
        const std::uint16_t idx = CalcLine(Y_D, Ystart_D, Xstart_U, Xstart_D) - 1;

        // Copy main line to bottom
        const std::uint16_t Delta_Y_M = Calc_Delta_Y(Line_Width, Y_U + idx, Y_D + idx, Len - idx); // Line Width_ Correction for a better line width
        for (std::uint16_t i = idx + 1; i < Len; i++)
          Y_D[i] = Y_U[i - idx] + Delta_Y_M;

        // Copy diagonal line to right
        const std::uint16_t Delta_Y_D = Y_U[Len - idx - 1] - Y_U[0];
        for (std::uint16_t i = 0; i <= idx; i++)
          Y_U[Len - idx + i - 1] = Y_D[i] + Delta_Y_D;
      } else {
        // Calculate main line down
        Y_D[0] = Y_U[0] = Ystart_D;
        CalcLine(Y_D, Yend_D, Xstart_D, Xend_D);

        // Calculate left diagonally line
        const std::uint16_t idx = CalcLine(Y_U, Ystart_U, Xstart_D, Xstart_U) - 1;

        // Copy main line to top
        const std::uint16_t Delta_Y_M = Calc_Delta_Y(Line_Width, Y_D + idx, Y_U + idx, Len - idx); // Line Width_ Correction for a better line width
        for (std::uint16_t i = idx + 1; i < Len; i++)
          Y_U[i] = Y_D[i - idx] - Delta_Y_M;

        // Copy diagonal line to right
        const std::uint16_t Delta_Y_D = Y_D[Len - idx - 1] - Y_D[0];
        for (std::uint16_t i = 0; i <= idx; i++)
          Y_D[Len - idx + i - 1] = Y_U[i] + Delta_Y_D;
      }
    }

    for (std::uint16_t x = 0; x < Len; x++)
      for (std::uint16_t y = Y_U[x]; y <= Y_D[x]; y++)
        if (X_Wider_Y)
          this->DrawPoint(Xstart_U + x, y, 1, DisplayStyle::DotStyle::Around, drawColor);
        else
          this->DrawPoint(y, Xstart_U + x, 1, DisplayStyle::DotStyle::Around, drawColor);
  }
}

void Image::DrawPath(std::uint16_t X[], std::uint16_t Y[], std::uint8_t nuPpoints, std::uint8_t Line_width, LineStyle Line_Style, DrawColor drawColor) {
  for (std::uint8_t i = 1; i < nuPpoints; i++) {
    DrawLine(X[i - 1], Y[i - 1], X[i], Y[i], Line_width, Line_Style, drawColor);
  }
}

void Image::DrawPath(std::uint16_t X, std::uint16_t Y, std::uint16_t angle[], std::uint16_t radius[], std::uint8_t am_points, std::uint8_t Line_width, LineStyle Line_Style, DrawColor drawColor) {
  for (std::uint8_t i = 0; i < am_points; i++) {
    DrawLineAngle(X, Y, angle[i], radius[i], Line_width);
    X += round(radius[i] * sin(angle[i] * PI / 180));
    Y -= round(radius[i] * cos(angle[i] * PI / 180));
  }
}

void Image::DrawPoint(std::uint16_t xPoint, std::uint16_t yPoint, std::uint8_t Dot_Pixel, DotStyle Dot_Style, DrawColor drawColor) {
  if (xPoint > Width_ || yPoint > Height_) {
    if (DEBUG_ERROR) DEBUG_OUTPUT printf("DrawPoint Input exceeds the normal display range (xPoint = %u, yPoint = %u)\n", xPoint, yPoint);
    return;
  }

  std::uint16_t WidthUp = (Width_ + 7) / 8 * 8;
  int16_t XDir_Num, YDir_Num;
  if (Dot_Style == DotStyle::Around) {
    for (XDir_Num = 0; XDir_Num < Dot_Pixel; XDir_Num++) {
      for (YDir_Num = 0; YDir_Num < Dot_Pixel; YDir_Num++) {
        if (xPoint + XDir_Num - Dot_Pixel < 0 || yPoint + YDir_Num - Dot_Pixel < 0)
          break;

        std::uint8_t bitmask = 1 << (7 - ((xPoint + XDir_Num - Dot_Pixel) % 8));
        this->bitmap[((yPoint + YDir_Num - Dot_Pixel) * WidthUp + (xPoint + XDir_Num - Dot_Pixel)) / 8] &= ~bitmask;
        if (drawColor == DrawColor::Black)
          this->bitmap[((yPoint + YDir_Num - Dot_Pixel) * WidthUp + (xPoint + XDir_Num - Dot_Pixel)) / 8] |= bitmask;
      }
    }
  }/* else {
      for (XDir_Num = 0; XDir_Num <  Dot_Pixel; XDir_Num++) {
          for (YDir_Num = 0; YDir_Num <  Dot_Pixel; YDir_Num++) {
              this->bitmap[] = drawColor;Paint_SetPixel(xPoint + XDir_Num - 1, yPoint + YDir_Num - 1, drawColor);
          }
      }
  }*/
}

void Image::DrawCircle(std::uint16_t X_Center, std::uint16_t Y_Center, std::uint16_t Radius, std::uint8_t Line_width, DisplayStyle::DrawFill Draw_Fill, DisplayStyle::DrawColor drawColor) {
  if (Y_Center + Radius >= Height_ || X_Center + Radius > Width_ || (int16_t)Y_Center - Radius < 0 || (int16_t)X_Center - Radius < 0) {
    if (DEBUG_ERROR) DEBUG_OUTPUT printf("DrawCircle Input exceeds the normal display range (X_Center = %u, Y_Center = %u, Radius = %u)\n", X_Center, Y_Center, Radius);
    return;
  }

  //Draw a circle from(0, R) as a starting point
  int16_t XCurrent = 0;
  int16_t YCurrent = Radius;

  //Cumulative error,judge the next point of the logo
  int16_t Esp = 3 - (Radius << 1);

  int16_t sCountY;
  if (Draw_Fill == DrawFill::Full) {
    while (XCurrent <= YCurrent) { //Realistic circles
      for (sCountY = XCurrent; sCountY <= YCurrent; sCountY++) {
        DrawPoint(X_Center + XCurrent, Y_Center + sCountY, 1, DotStyleDft, drawColor);//1
        DrawPoint(X_Center - XCurrent, Y_Center + sCountY, 1, DotStyleDft, drawColor);//2
        DrawPoint(X_Center - sCountY, Y_Center + XCurrent, 1, DotStyleDft, drawColor);//3
        DrawPoint(X_Center - sCountY, Y_Center - XCurrent, 1, DotStyleDft, drawColor);//4
        DrawPoint(X_Center - XCurrent, Y_Center - sCountY, 1, DotStyleDft, drawColor);//5
        DrawPoint(X_Center + XCurrent, Y_Center - sCountY, 1, DotStyleDft, drawColor);//6
        DrawPoint(X_Center + sCountY, Y_Center - XCurrent, 1, DotStyleDft, drawColor);//7
        DrawPoint(X_Center + sCountY, Y_Center + XCurrent, 1, DotStyleDft, drawColor);
      }
      if (Esp < 0)
        Esp += 4 * XCurrent + 6;
      else {
        Esp += 10 + 4 * (XCurrent - YCurrent);
        YCurrent--;
      }
      XCurrent++;
    }
  } else { //Draw a hollow circle
    if (Line_width > 1) {
      int16_t XCurrent2 = 0;
      int16_t YCurrent2 = Radius - Line_width;
      int16_t Esp2 = 3 - ((Radius - Line_width) << 1);

      while (XCurrent <= YCurrent) {
        for (std::uint16_t I = 0; I < YCurrent - YCurrent2; I++) {
          DrawPoint(X_Center + XCurrent, Y_Center + YCurrent - I, 1, DotStyleDft, drawColor);
          DrawPoint(X_Center - XCurrent, Y_Center + YCurrent - I, 1, DotStyleDft, drawColor);
          DrawPoint(X_Center - YCurrent + I, Y_Center + XCurrent, 1, DotStyleDft, drawColor);
          DrawPoint(X_Center - YCurrent + I, Y_Center - XCurrent, 1, DotStyleDft, drawColor);
          DrawPoint(X_Center - XCurrent, Y_Center - YCurrent + I, 1, DotStyleDft, drawColor);
          DrawPoint(X_Center + XCurrent, Y_Center - YCurrent + I, 1, DotStyleDft, drawColor);
          DrawPoint(X_Center + YCurrent - I, Y_Center - XCurrent, 1, DotStyleDft, drawColor);
          DrawPoint(X_Center + YCurrent - I, Y_Center + XCurrent, 1, DotStyleDft, drawColor);
        }

        if (Esp < 0)
          Esp += 4 * XCurrent + 6;
        else {
          Esp += 10 + 4 * (XCurrent - YCurrent);
          YCurrent--;
        }
        XCurrent++;

        if (Esp2 < 0)
          Esp2 += 4 * XCurrent2 + 6;
        else {
          Esp2 += 10 + 4 * (XCurrent2 - YCurrent2);
          YCurrent2--;
        }
        XCurrent2++;
      }
    } else {
      while (XCurrent <= YCurrent) {
        DrawPoint(X_Center + XCurrent, Y_Center + YCurrent, 1, DotStyleDft, drawColor);
        DrawPoint(X_Center - XCurrent, Y_Center + YCurrent, 1, DotStyleDft, drawColor);
        DrawPoint(X_Center - YCurrent, Y_Center + XCurrent, 1, DotStyleDft, drawColor);
        DrawPoint(X_Center - YCurrent, Y_Center - XCurrent, 1, DotStyleDft, drawColor);
        DrawPoint(X_Center - XCurrent, Y_Center - YCurrent, 1, DotStyleDft, drawColor);
        DrawPoint(X_Center + XCurrent, Y_Center - YCurrent, 1, DotStyleDft, drawColor);
        DrawPoint(X_Center + YCurrent, Y_Center - XCurrent, 1, DotStyleDft, drawColor);
        DrawPoint(X_Center + YCurrent, Y_Center + XCurrent, 1, DotStyleDft, drawColor);

        if (Esp < 0)
          Esp += 4 * XCurrent + 6;
        else {
          Esp += 10 + 4 * (XCurrent - YCurrent);
          YCurrent--;
        }
        XCurrent++;
      }
    }
  }
}

void Image::DrawCirclePart(std::uint16_t X_Center, std::uint16_t Y_Center, std::uint16_t Radius, std::uint16_t startangle, std::uint16_t endangle, std::uint8_t Line_width) {
  double dstartangle = startangle * PI / 180;
  double dendangle = endangle * PI / 180;

  const std::uint8_t full_angle[] = {
    startangle == 0 && endangle >= 45,
    startangle <= 45 && endangle >= 90,
    startangle <= 90 && endangle >= 135,
    startangle <= 135 && endangle >= 180,
    startangle <= 180 && endangle >= 225,
    startangle <= 225 && endangle >= 270,
    startangle <= 270 && endangle >= 315,
    startangle <= 315 && endangle == 360,
  };

  const std::uint8_t start_area[] = {
                         startangle < 45,
    startangle >= 45 && startangle < 90,
    startangle >= 90 && startangle < 135,
    startangle >= 135 && startangle < 180,
    startangle >= 180 && startangle < 225,
    startangle >= 225 && startangle < 270,
    startangle >= 270 && startangle < 315,
    startangle >= 315 && startangle < 360,
  };

  const std::uint8_t end_area[] = {
                         endangle < 45,
    endangle >= 45 && endangle < 90,
    endangle >= 90 && endangle < 135,
    endangle >= 135 && endangle < 180,
    endangle >= 180 && endangle < 225,
    endangle >= 225 && endangle < 270,
    endangle >= 270 && endangle < 315,
    endangle >= 315 && endangle < 360,
  };

  const double dstartangles[] = {
    Radius * sin(dstartangle),
    Radius * cos(dstartangle),
    -Radius * cos(dstartangle),
    Radius * sin(dstartangle),
    -Radius * sin(dstartangle),
    -Radius * cos(dstartangle),
    Radius * cos(dstartangle),
    -Radius * sin(dstartangle)
  };

  const double dendangles[] = {
    Radius * sin(dendangle),
    Radius * cos(dendangle),
    -Radius * cos(dendangle),
    Radius * sin(dendangle),
    -Radius * sin(dendangle),
    -Radius * cos(dendangle),
    Radius * cos(dendangle),
    -Radius * sin(dendangle)
  };

  int16_t f = 1 - Radius;
  int16_t ddF_x = 0;
  int16_t ddF_y = -2 * Radius;
  int16_t x = 0;
  int16_t y = Radius;

  int16_t f2 = 1 - (Radius - Line_width);
  int16_t ddF_x2 = 0;
  int16_t ddF_y2 = -2 * (Radius - Line_width);
  int16_t x2 = 0;
  int16_t y2 = Radius - Line_width;

  while (x <= y) {
    for (std::uint16_t I = 0; I < y - y2; I++) {
      if (full_angle[0] || ((start_area[0] && end_area[0]) ? (x >= dstartangles[0] && x <= dendangles[0]) : ((start_area[0] && x >= dstartangles[0]) || (end_area[0] && x <= dendangles[0])))) // 0 - 45°
        this->SetPixel(X_Center + x, Y_Center - y + I);
      if (full_angle[1] || ((start_area[1] && end_area[1]) ? (x <= dstartangles[1] && x >= dendangles[1]) : ((start_area[1] && x <= dstartangles[1]) || (end_area[1] && x >= dendangles[1])))) // 45° - 90°
        this->SetPixel(X_Center + y - I, Y_Center - x);
      if (full_angle[2] || ((start_area[2] && end_area[2]) ? (x >= dstartangles[2] && x <= dendangles[2]) : ((start_area[2] && x >= dstartangles[2]) || (end_area[2] && x <= dendangles[2])))) // 90° - 135°
        this->SetPixel(X_Center + y - I, Y_Center + x);
      if (full_angle[3] || ((start_area[3] && end_area[3]) ? (x <= dstartangles[3] && x >= dendangles[3]) : ((start_area[3] && x <= dstartangles[3]) || (end_area[3] && x >= dendangles[3])))) // 135° - 180°
        this->SetPixel(X_Center + x, Y_Center + y - I);
      if (full_angle[4] || ((start_area[4] && end_area[4]) ? (x >= dstartangles[4] && x <= dendangles[4]) : ((start_area[4] && x >= dstartangles[4]) || (end_area[4] && x <= dendangles[4])))) // 180° - 225°
        this->SetPixel(X_Center - x, Y_Center + y - I);
      if (full_angle[5] || ((start_area[5] && end_area[5]) ? (x <= dstartangles[5] && x >= dendangles[5]) : ((start_area[5] && x <= dstartangles[5]) || (end_area[5] && x >= dendangles[5])))) // 225° - 270°
        this->SetPixel(X_Center - y + I, Y_Center + x);
      if (full_angle[6] || ((start_area[6] && end_area[6]) ? (x >= dstartangles[6] && x <= dendangles[6]) : ((start_area[6] && x >= dstartangles[6]) || (end_area[6] && x <= dendangles[6])))) // 270° - 315°
        this->SetPixel(X_Center - y + I, Y_Center - x);
      if (full_angle[7] || ((start_area[7] && end_area[7]) ? (x <= dstartangles[7] && x >= dendangles[7]) : ((start_area[7] && x <= dstartangles[7]) || (end_area[7] && x >= dendangles[7])))) // 315° - 360°
        this->SetPixel(X_Center - x, Y_Center - y + I);
      if (Line_width == 1)
        break;
    }

    if (f >= 0) {
      y--;
      ddF_y += 2;
      f += ddF_y;
    }
    x++;
    ddF_x += 2;
    f += ddF_x + 1;

    if (f2 >= 0) {
      y2--;
      ddF_y2 += 2;
      f2 += ddF_y2;
    }
    x2++;
    ddF_x2 += 2;
    f2 += ddF_x2 + 1;
  }
}

void Image::DrawRectWH(std::uint16_t Left, std::uint16_t Top, int16_t Width_, int16_t Height_, std::uint8_t Line_width, LineStyle Line_Style, DrawFill Draw_Fill, DrawColor drawColor) {
  if (Width_ < 0) {
    Left += Width_;
    Width_ = -Width_;
  }
  if (Height_ < 0) {
    Top += Height_;
    Height_ = -Height_;
  }
  DrawRect(Left, Top, Left + Width_, Top + Height_, Line_width, Line_Style, Draw_Fill, drawColor);
}

void Image::DrawRectWH(std::uint16_t Left, std::uint16_t Top, int16_t Width_, int16_t Height_, DisplayStyle::DrawColor drawColor) {
  DrawRectWH(Left, Top, Width_, Height_, 1, LineStyle::Solid, DrawFill::Full, drawColor);
}

void Image::DrawRect(std::uint16_t X1, std::uint16_t Y1, std::uint16_t X2, std::uint16_t Y2, std::uint8_t Line_width, LineStyle Line_Style, DrawFill Draw_Fill, DrawColor drawColor) {
  if (X1 >= Width_ || X2 >= Width_ || Y1 >= Height_ || Y2 >= Height_) {
    if (DEBUG_ERROR) DEBUG_OUTPUT printf("DrawRect Input exceeds the normal display range (X1 = %u, Y1 = %u, X2 = %u, Y2 = %u)\n", X1, Y1, X2, Y2);
    return;
  }
  if (Draw_Fill == DrawFill::Full) {
    const auto NextColor = [drawColor]() {
      if (drawColor == DisplayStyle::DrawColor::Gray) {
        static bool grayCounter = false;
        grayCounter = !grayCounter;
        return grayCounter ? DisplayStyle::DrawColor::Black : DisplayStyle::DrawColor::White;
      } else
        return drawColor;
    };

    for (std::uint16_t x = X1; x <= X2; x++) {
      for (std::uint16_t y = Y1; y <= Y2; y++) {
        this->DrawPoint(x, y, 1, DisplayStyle::DotStyle::Around, NextColor());
      }
      if (drawColor == DrawColor::Gray && (Y2 - Y1) % 2 == 1)
        NextColor();
    }
  } else {
    this->DrawLine(X1, Y1, X2, Y1, Line_width, Line_Style, drawColor);
    this->DrawLine(X2, Y1, X2, Y2, Line_width, Line_Style, drawColor);
    this->DrawLine(X1, Y2, X2, Y2, Line_width, Line_Style, drawColor);
    this->DrawLine(X1, Y1, X1, Y2, Line_width, Line_Style, drawColor);
  }
}

void Image::DrawRect(std::uint16_t X1, std::uint16_t Y1, std::uint16_t X2, std::uint16_t Y2, DisplayStyle::DrawColor drawColor) {
  DrawRect(X1, Y1, X2, Y2, 1, LineStyle::Solid, DrawFill::Full, drawColor);
}

void Image::DrawTriangle(std::uint16_t X1, std::uint16_t Y1, std::uint16_t X2, std::uint16_t Y2, std::uint16_t X3, std::uint16_t Y3, std::uint8_t Line_width, LineStyle Line_Style, DrawFill Draw_Fill, DrawColor drawColor) {
  if (X1 >= Width_ || X2 >= Width_ || X3 >= Width_ || Y1 >= Height_ || Y2 >= Height_ || Y3 >= Height_) {
    if (DEBUG_ERROR) DEBUG_OUTPUT printf("DrawTriangle Input exceeds the normal display range (X1 = %u, Y1 = %u, X2 = %u, Y2 = %u, X3 = %u, Y3 = %u)\n", X1, Y1, X2, Y2, X3, Y3);
    return;
  }
  if (Draw_Fill == DrawFill::Full) {
    if (DEBUG_ERROR) DEBUG_OUTPUT printf("Achtung, bisher keine Dreiecksfunktion, die Dreiecke füllt!!\n");
  } else {
    this->DrawLine(X1, Y1, X2, Y2, Line_width, Line_Style, drawColor);
    this->DrawLine(X2, Y2, X3, Y3, Line_width, Line_Style, drawColor);
    this->DrawLine(X3, Y3, X1, Y1, Line_width, Line_Style, drawColor);
  }
}
