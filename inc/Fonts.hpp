/**
 * @file Fonts.hpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Delivers the fonts in multiple sizes
 * @version 2.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#pragma once
#include <cstdint>
#include <stdio.h>
#include <memory>
#include <vector>

namespace Graphics {
  constexpr std::uint8_t NumberOfFonts = 11; // Number of fonts
  constexpr std::uint8_t MaxStoredFont = 4; // Biggest font which is stored

  enum class FontSize {
    /* Stored fonts */
    S8 = 0,
    S12 = 1,
    S16 = 2,
    S20 = 3,
    S24 = 4,

    /* Automatically scaled fonts */
    G36 = 5,
    G48 = 6,
    G60 = 7,
    G72 = 8,
    G84 = 9,
    G96 = 10
  };

  /* Internal font-class */
  class Font {
  public:
    Font(std::uint8_t height, std::uint8_t width, FontSize size);
    const std::uint16_t Width;
    const std::uint16_t Height;
    const FontSize fontSize;
    const Font* bigger;
    const Font* smaller;
    const Font* Smaller() { return smaller != NULL ? smaller : this; }
    const Font* Bigger() { return bigger != NULL ? bigger : this; }
    std::unique_ptr<std::uint8_t[]> getFont(char c);
    const std::uint8_t* getFontData(std::uint8_t c) const;
    std::uint8_t getFontSizeUint() const;
    static std::uint8_t fontSizeToUint(FontSize fontSize);

  private:
  };

  /* Internal font-list-class */
  class Fonts {
  public:
    static Fonts& getInstance();
    Font& getFont(FontSize fontSize) const;
    std::uint8_t Height(FontSize fontSize);
    std::uint8_t Width(FontSize fontSize, char c);
    std::uint16_t Width(FontSize fontSize, std::vector<char>& c);

    static void ReplaceSpecialChars(std::vector<char>& str);

  private:
    Fonts();

    Font* list_[NumberOfFonts];
  };

  /* External font-class */
  class cFont {
  public:
    const std::uint8_t* Table;
    std::uint16_t Width;
    std::uint16_t Height;
  };
};
