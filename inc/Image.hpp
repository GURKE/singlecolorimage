/**
 * @file Image.hpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Black white image class containing multiple functions for drawing lines, circles or text
 * @version 2.0
 * @date 2023-11-23
 *
 * Remarks:
*/
#pragma once
#include <memory> // std-ptr
#include <vector>

#include "Debug.h"
#include "Fonts.hpp"

namespace Graphics {
  namespace DisplayStyle {
    enum class DotStyle { Around, Rightup };
    constexpr DotStyle DotStyleDft = DotStyle::Around;
    enum class LineStyle { Solid, Dotted };
    enum class DrawFill { Empty, Full };
    enum class DrawColor { White, Black, Gray };
    enum class AlignHor { Left, Center, Right };
    enum class AlignVer { Top, Center, Bottom };
    enum class DrawStyle { Default, Underlined };
  };

  class Box {
  public:
    Box(std::uint16_t _Width, std::uint16_t _Height) : Width(_Width), Height(_Height) {}
    Box(std::uint16_t _Left, std::uint16_t _Top, std::uint16_t _Width, std::uint16_t _Height) : Left(_Left), Top(_Top), Width(_Width), Height(_Height) {}

    const std::uint16_t Left = 0, Top = 0;
    const std::uint16_t Width = 0, Height = 0;
  };

  enum class Mergemode { Replace, Add };

  class Image {
  public:
    /**
     * @brief Construct a new Image with given size
     *
     * @param width Number of pixel in width
     * @param height Number of pixel in height
    */
    Image(std::uint16_t width, std::uint16_t height);

    /**
     * @brief Construct a new image out of a text, the image has exactly the size needed for printing the string
     *
     * @param pString Text to print
     * @param Font font of the text
     * @param Color Color of the printed text
    */
    Image(const char* pString, Font& font, DisplayStyle::DrawColor Color);

    ~Image();

    /**
     *  @brief Saves the image on the filesystem
     *
     *  @param filename Filename of the picture
     *  @return True if saving was successfull
     */
    virtual bool SaveImage(const char* filename) const = 0;

    /**
     * @brief Get the Size of a BMP picture
     *
     * @param filename Filename of the BMP picture
     * @retval Box and size of the picture
    */
    virtual Box getSize(const char* filename) const = 0;


    /******************** Image manipulations & operations functions ********************/
    /**
     *  @brief  Clears the image so every bit is 0 (= white)
    */
    void Clear();

    /**
     * @brief Draws a pixel black
     *
     * @param x Position of the pixel in X-Direction
     * @param y Position of the pixel in Y-Direction
    */
    void SetPixel(std::uint16_t x, std::uint16_t y);

    /**
     * @brief Merges another picture
     *
     * @param img Picture to merge
     * @param xPoint Position of the merged picture in X-Direction (in Pixel)
     * @param yPoint Position of the merged picture in Y-Direction (in Pixel)
     * @param mode Replace pixels or add new black pixels to image
    */
    void Merge(std::shared_ptr<Image> img, std::uint16_t xPoint = 0, std::uint16_t yPoint = 0, Mergemode mode = Mergemode::Replace);

    enum class Rotation { None, Counterclockwise, Clockwise, Half };
    enum class Mirroring { None, Vertical, Horizontal };

    /**
     * @brief Rotate image
     *
     * @param rotation Rotation options
    */
    void Rotate(const Rotation rotation);

    /**
     * @brief Mirror image
     *
     * @param mirror Mirror option
    */
    void Mirror(const Mirroring mirror);


    /******************** Draw Functions ********************/
    /**
     * @brief Draws a single char at a given position
     *
     * @param xPoint X coordinate
     * @param yPoint Y coordinate
     * @param asciiChar The char to print
     * @param font Reference to the char's font
    */
    void DrawChar(std::uint16_t xPoint, std::uint16_t yPoint, const char asciiChar, const Font& font);

    /**
     * @brief Use the 8-point method to draw a circle of the specified size at the specified position
     *
     * @param X_Center Center X coordinate
     * @param Y_Center Center Y coordinate
     * @param Radius Circle Radius
     * @param Line_width Line width
     * @param Draw_Fill Whether to fill the inside of the Circle
     * @param Color The color of the circle segment
     */
    void DrawCircle(std::uint16_t X_Center, std::uint16_t Y_Center, std::uint16_t Radius, std::uint8_t Line_width = 1, DisplayStyle::DrawFill Draw_Fill = DisplayStyle::DrawFill::Empty, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);

    /**
     * @brief Use the 8-point method to draw a circle part at the specified position. The circle will be drawn from startangle clockwise until endangle.
     *
     * @param X_Center Center X coordinate
     * @param Y_Center Center Y coordinate
     * @param Radius Circle Radius
     * @param startangle In Degree, Top is 0°
     * @param endangle In Degree, Top is 0°
     * @param Line_width Line width
    */
    void DrawCirclePart(std::uint16_t X_Center, std::uint16_t Y_Center, std::uint16_t Radius, std::uint16_t startangle, std::uint16_t endangle, std::uint8_t Line_width = 1);

    /**
     * @brief Draws a line between two points
     *
     * @param Xstart X-Coordinate of Startpoint
     * @param Ystart Y-Coordinate of Startpoint
     * @param Xend X-Coordinate of Endpoint
     * @param Yend Y-Coordinate of Endpoint
     * @param Line_Width Width_ of the line in Pixel
     * @param Line_Style Full line or dashed - NOT IMPLEMENTED YET!
     * @param Color Color of the line
     */
    void DrawLine(std::uint16_t Xstart, std::uint16_t Ystart, std::uint16_t Xend, std::uint16_t Yend, std::uint8_t Line_Width = 1, DisplayStyle::LineStyle Line_Style = DisplayStyle::LineStyle::Solid, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);

    /**
     * @brief Draws a crooked line by a start coordinate, a radius and an angle
     *
     * @param Xstart X-Coordinate of Start
     * @param Ystart Y-Coordinate of Start
     * @param angle Angle of the line (0°: vertical upwards, clockwise)
     * @param Radius Length of the line
     * @param Line_Width Width_ of the line
     * @param Line_Style Full line or dashed - NOT IMPLEMENTED YET!
     * @param Color Color of the line
     */
    void DrawLineAngle(std::uint16_t Xstart, std::uint16_t Ystart, std::uint16_t angle, std::uint16_t Radius, std::uint8_t Line_Width = 1, DisplayStyle::LineStyle Line_Style = DisplayStyle::LineStyle::Solid, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawPath(std::uint16_t X[], std::uint16_t Y[], std::uint8_t nuPpoints, std::uint8_t Line_width, DisplayStyle::LineStyle Line_Style = DisplayStyle::LineStyle::Solid, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawPath(std::uint16_t X, std::uint16_t Y, std::uint16_t angle[], std::uint16_t radius[], std::uint8_t am_points, std::uint8_t Line_width, DisplayStyle::LineStyle Line_Style = DisplayStyle::LineStyle::Solid, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);

    /**
     * @brief Draws a point
     *
     * @param Xpoint The Xpoint coordinate of the point
     * @param Ypoint The Ypoint coordinate of the point
     * @param Dot_Pixel Point size
     * @param Dot_Style Point Style
     * @param Color Painted color
     */
    void DrawPoint(std::uint16_t Xpoint, std::uint16_t Ypoint, std::uint8_t Dot_Pixel = 1, DisplayStyle::DotStyle Dot_Style = DisplayStyle::DotStyleDft, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawRect(std::uint16_t X1, std::uint16_t Y1, std::uint16_t X2, std::uint16_t Y2, std::uint8_t Line_width, DisplayStyle::LineStyle Line_Style = DisplayStyle::LineStyle::Solid, DisplayStyle::DrawFill Draw_Fill = DisplayStyle::DrawFill::Empty, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawRect(std::uint16_t X1, std::uint16_t Y1, std::uint16_t X2, std::uint16_t Y2, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawRectWH(std::uint16_t Left, std::uint16_t Top, int16_t Width, int16_t Height, std::uint8_t Line_width = 1, DisplayStyle::LineStyle Line_Style = DisplayStyle::LineStyle::Solid, DisplayStyle::DrawFill Draw_Fill = DisplayStyle::DrawFill::Empty, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawRectWH(std::uint16_t Left, std::uint16_t Top, int16_t Width, int16_t Height, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawString(std::uint16_t Xstart, std::uint16_t Ystart, const char* pString, Font& font, DisplayStyle::AlignHor Align_LR = DisplayStyle::AlignHor::Left, DisplayStyle::AlignVer Align_TB = DisplayStyle::AlignVer::Top, DisplayStyle::DrawStyle Style = DisplayStyle::DrawStyle::Default, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawStringV(std::uint16_t Xstart, std::uint16_t Ystart, std::vector<char>* pString, Font& font, DisplayStyle::AlignHor Align_LR = DisplayStyle::AlignHor::Left, DisplayStyle::AlignVer Align_TB = DisplayStyle::AlignVer::Top, DisplayStyle::DrawStyle Style = DisplayStyle::DrawStyle::Default, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);
    void DrawTriangle(std::uint16_t X1, std::uint16_t Y1, std::uint16_t X2, std::uint16_t Y2, std::uint16_t X3, std::uint16_t Y3, std::uint8_t Line_width = 1, DisplayStyle::LineStyle Line_Style = DisplayStyle::LineStyle::Solid, DisplayStyle::DrawFill DrawFill = DisplayStyle::DrawFill::Empty, DisplayStyle::DrawColor Color = DisplayStyle::DrawColor::Black);

    std::unique_ptr<std::uint8_t[]> bitmap;

  protected:
    std::uint16_t Width_;
    std::uint16_t Height_;

    Image();

    /**
     * @brief Returns the width of the image in bytes
     *
     * @retval std::uint16_t
    */
    std::uint16_t WidthBytes() const { return (Width_ + 7) / 8; }

  private:
    void Merge(std::uint16_t Xpoint, std::uint16_t Ypoint, std::unique_ptr<std::uint8_t[]> _bitmap, Mergemode _mode, std::uint16_t _Width, std::uint16_t _Height);

    /**
     * @brief Calculates the Y-Coordinates of a crooked line
     *
     * @param Y Array of the calculated Y-Coordinates already including the Y-Start-Coordinate
     * @param Yend Y-Coordinate of Endpoint
     * @param Xstart X-Coordinate of Startpoint
     * @param Xend X-Coordinate of Endpoint
     * @return std::uint16_t returns the number of needed X-Pixels
     */
    std::uint16_t CalcLine(std::uint16_t* Y, std::uint16_t Yend, std::uint16_t Xstart, std::uint16_t Xend);

    /**
     * @brief Calculates the horizontal width of a crooked line when given vertical width is given
     *
     * @param Delta_Y Given vertical width
     * @param Y Array of Y-Coordinates
     * @param Len Length of the Array
     * @return std::uint16_t Horizontal Width_
     */
    std::uint16_t Calc_Delta_X(const std::uint16_t Delta_Y, const std::uint16_t* Y, const std::uint16_t Len);

    /**
     * @brief Calculates the necessary vertical width to reach the requested Line Width_
     *
     * @param Line_Width Requested Line Width_
     * @param Y_U Array of Y-Coordinates of horizontal line
     * @param Y_D Array of Y-Coordinates of vertical line
     * @param Len Length of the arrays
     * @return std::uint16_t Calculated vertical Width_
     */
    std::uint16_t Calc_Delta_Y(std::uint8_t Line_Width, const std::uint16_t* Y_U, const std::uint16_t* Y_D, std::uint16_t Len);
  };
};